import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+10+40";
        assertEquals("450", calculatorResource.calculate(expression));

        expression = " 300-100-50-50 ";
        assertEquals("100", calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300+2";
        assertEquals(402, calculatorResource.sum(expression));

        expression = "300+99+1";
        assertEquals(400, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "300-200-105";
        assertEquals(-5, calculatorResource.subtraction(expression));

        expression = "300-99-1";
        assertEquals(200, calculatorResource.subtraction(expression));

    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "99*10*10";
        assertEquals(9900, calculatorResource.multiplication(expression));

        expression = "20*2*100";
        assertEquals(4000, calculatorResource.multiplication(expression));
    }
    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "150/10/15";
        assertEquals(1, calculatorResource.division(expression));

        expression = "300/15/10";
        assertEquals(2, calculatorResource.division(expression));

        /*
        expression = "kjb";
        assertEquals("Du har tastet inn et ugyldig utrykk", calculatorResource.division(expression));
        */

    }


}
